package it.dtk.stackoverflow

import it.dtk.stackoverflow.Model.{Post, User}
import org.joda.time.{IllegalInstantException, DateTimeZone, DateTime}
import org.joda.time.format.DateTimeFormat

import scala.util.Try
import scala.xml.{NodeSeq, Elem}

/**
 * Created by fabiofumarola on 21/02/15.
 */
object Loaders {

  /**
   * Created by fabiofumarola on 17/02/15.
   */
  trait XmlLoader[T] {

    val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").withZoneUTC()

    def getInt(n: NodeSeq): Option[Int] =
      n.text match {
        case "" => None
        case x  => Some(x.toInt)
      }

    def getDatetime(n: NodeSeq): Option[DateTime] =
      n.text match {
        case "" => None
        case s  =>
          try {
            Some(dateFormat.parseDateTime(s))
          } catch {
            case _: Exception =>
              None
          }

      }

    def getString(n: NodeSeq): Option[String] =
      n.text match {
        case "" => None
        case s  => Some(s)
      }

    def parse(s: String): Option[T] =
      if (s.startsWith("  <row ")) Some(parseXml(scala.xml.XML.loadString(s)))
      else None

    def parseXml(node: Elem): T
  }


  /**
   * Created by fabiofumarola on 17/02/15.
   */
  object UserLoader extends XmlLoader[User] {

    override def parseXml(n: Elem): User =
      User(
        id = getInt(n \ "@Id"),
        reputation = getInt(n \ "@Reputation"),
        creationDate = getDatetime(n \ "@CreationDate"),
        displayName = getString(n \ "@DisplayName"),
        lastAccessDate = getDatetime(n \ "@LastAccessDate"),
        websiteUrl = getString(n \ "@WebsiteUrl"),
        location = getString(n \ "@Location"),
        aboutMe = getString(n \ "@AboutMe"),
        views = getInt(n \ "@Views"),
        upVotes = getInt(n \ "@UpVotes"),
        downVotes = getInt(n \ "@DownVotes"),
        profileImageUrl = getString(n \ "@ProfileImageUrl"),
        age = getInt(n \ "@Age"),
        accountId = getInt(n \ "@accountId")
      )
  }

  object PostLoader extends XmlLoader[Post] {

    override def parseXml(n: Elem): Post =
      Post(
        id = getInt(n \ "@Id"),
        postTypeId = getInt(n \ "@PostTypeId"),
        parentId = getInt(n \ "@ParentId"),
        acceptedAnswerId = getInt(n \ "@AcceptedAnswerId"),
        creationDate = getDatetime(n \ "@CreationDate"),
        score = getInt(n \ "@Score"),
        viewCount = getInt(n \ "@ViewCount"),
        body = getString(n \ "@Body"),
        ownerUserId = getInt(n \ "@OwnerUserId"),
        lastEditorUserId = getInt(n \ "@LastEditorUserId"),
        lastEditDate = getDatetime(n \ "@LastEditDate"),
        lastActivityDate = getDatetime(n \ "@LastActivityDate"),
        commentCount = getInt(n \ "@CommentCount")
      )
  }
}
