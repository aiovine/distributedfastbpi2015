package it.dtk.stackoverflow

import org.joda.time.DateTime

/**
 * Created by fabiofumarola on 17/02/15.
 */
object Model extends Serializable {

  case class User(
                   id: Option[Int],
                   reputation: Option[Int] = None,
                   creationDate: Option[DateTime] = None,
                   displayName: Option[String] = None,
                   lastAccessDate: Option[DateTime] = None,
                   websiteUrl: Option[String] = None,
                   location: Option[String] = None,
                   aboutMe: Option[String] = None,
                   views: Option[Int] = None,
                   upVotes: Option[Int] = None,
                   downVotes: Option[Int] = None,
                   profileImageUrl: Option[String] = None,
                   age: Option[Int] = None,
                   accountId: Option[Int] = None)

  case class Post(
                   id: Option[Int] = None,
                   postTypeId: Option[Int] = None,
                   parentId: Option[Int] = None,
                   acceptedAnswerId: Option[Int] = None,
                   creationDate: Option[DateTime] = None,
                   score: Option[Int] = None,
                   viewCount: Option[Int] = None,
                   body: Option[String] = None,
                   ownerUserId: Option[Int] = None,
                   lastEditorUserId: Option[Int] = None,
                   lastEditDate: Option[DateTime] = None,
                   lastActivityDate: Option[DateTime] = None,
                   commentCount: Option[Int] = None)

  /**
   *
   * @param key QuestionId:AnswerId
   * @param post
   * @param user
   */
  case class SequenceElem(
                           key: Option[String],
                           post: Post,
                           user: Option[User])

  case class Answer(
                         reputation: Int,
                         creationDate: Long,
                         displayName: Int,
                         lastAccessDate: Long,
                         websiteUrl: Int,
                         location: Int,
                         aboutMe: Int,
                         views: Int,
                         upVotes: Int,
                         downVotes: Int,
                         profileImageUrl: Int,
                         age: Int,
                         postScore: Int,
                         postViewCount: Int,
                         commentCount: Int,
                         classificationValue: Int,
                         regressionValue: Double,
                         sid: Int,
                         answerId: Int)
}

