package it.dtk.stackoverflow

import it.dtk.stackoverflow.Loaders._
import it.dtk.stackoverflow.Model._
import it.dtk.stackoverflow.hbase.HBaseAdminUtils
import it.dtk.stackoverflow.hbase.HbaseConverter._
import it.dtk.stackoverflow.hbase.ModelDAO.PostDAO
import it.nerdammer.spark.hbase._
import org.apache.spark.rdd._
import org.apache.spark.{SparkConf, SparkContext}


/**
* Created by fabiofumarola on 21/02/15.
*/

object StackExchange extends Serializable {

  val userTableName = "users"
  val userCfs = List("info")

  val postTableName = "posts"
  val postCfs = List("info")

  val sequenceTableName = "sequences"
  val sequenceCFfs = List("post", "user")

  def init(usersPath: String, postsPath: String)(implicit sc: SparkContext): Unit = {
    initUsers(usersPath)
    initPost(postsPath)
    initSequences(sc)
  }

  private def initUsers(inputPath: String)(implicit sc: SparkContext): Unit = {

    HBaseAdminUtils.createTable(userTableName, userCfs)
    val users = sc.textFile(inputPath).flatMap(UserLoader.parse)
    users.toHBaseTable(userTableName).inColumnFamily(userCfs(0)).save()
  }

  /**
   * save post of type 1 and 2
   * @param inputPath
   * @return
   */
  private def initPost(inputPath: String)(implicit sc: SparkContext): Unit = {
    HBaseAdminUtils.createTable(postTableName, postCfs)
    val posts = sc.textFile(inputPath).flatMap(PostLoader.parse)
    val filteredPost = posts.filter(p => p.postTypeId.get == 1 || p.postTypeId.get == 2)

    val postWithUserId = filteredPost.filter { p =>

      if (p.postTypeId.get == 2 && p.ownerUserId.isEmpty)
        false
      else
        true

    }
    postWithUserId.toHBaseTable(postTableName).inColumnFamily(postCfs(0)).save()
  }

  private def initSequences(implicit sc: SparkContext): Unit = {

    HBaseAdminUtils.createTable(sequenceTableName, sequenceCFfs)

    val users = sc.hbaseTable[User](userTableName).inColumnFamily(userCfs(0))
    val posts = sc.hbaseTable[Post](postTableName).inColumnFamily(postCfs(0))

    val elementsWithoutUser = posts.
      filter(_.ownerUserId.isEmpty).map(p => SequenceElem(PostDAO.generateKey(p), p, None))

    val keyPost = new PairRDDFunctions(posts.keyBy(_.ownerUserId))
    val keyUsers = users.keyBy(_.id)

    val joinedByUserId = keyPost.join(keyUsers)

    val sequenceElems = joinedByUserId.map {
      case (userId, (post, user)) =>
        SequenceElem(PostDAO.generateKey(post), post, Some(user))
    } ++ elementsWithoutUser

    sequenceElems.toHBaseTable(sequenceTableName).save()

  }

}

object Main {

  def main(args: Array[String]): Unit = {

    val usersPath = "src/test/resources/Users.xml"
    val postsPath = "src/test/resources/Posts.xml"

    val sparkConf = new SparkConf()
    sparkConf.set("spark.hbase.host", "localhost")
    implicit val sc = new SparkContext("local", "Main", sparkConf)
    StackExchange.init(usersPath, postsPath)

    //val dataset = StackExchangeDataset.generateSequenceDataset(2)
    //dataset.foreach(println)

    sc.stop()
  }
}
