package it.dtk.stackoverflow

import it.dtk.stackoverflow.Model.{Answer, SequenceElem}
import it.dtk.stackoverflow.hbase.HbaseConverter._
import it.nerdammer.spark.hbase._
import org.apache.spark.rdd._
import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.DateTime
import org.json4s.{Extraction, DefaultFormats}
import org.json4s.jackson.JsonMethods._


/**
 * Created by fabiofumarola on 21/02/15.
 */


object StackExchangeDataset extends Serializable {
  val userTableName = "users"
  val userCfs = List("info")

  val postTableName = "posts"
  val postCfs = List("info")

  val sequenceTableName = "sequences"
  val sequenceCFfs = List("post", "user")

  implicit val formats = DefaultFormats // Brings in default date formats etc for Json4s


  private def getLastCellElement(elem: SequenceElem): String ={
    val user = elem.user.get
    val post =elem.post
    val answerId = post.id.getOrElse(0)
    val reputation = user.reputation.getOrElse(0)
    val creationDate = post.creationDate.get.getMillis
    val displayName = convertToInt(user.displayName.getOrElse("0"))
    val lastAccessDate = user.lastAccessDate.getOrElse(DateTime.now()).getMillis
    val webSiteUrl = convertToInt(user.websiteUrl.getOrElse("0"))
    val location = convertToInt(user.location.getOrElse("0"))
    val aboutMe = convertToInt(user.aboutMe.getOrElse("0"))
    val views = user.views.getOrElse(0)
    val upVotes = user.upVotes.getOrElse(0)
    val downVotes = user.downVotes.getOrElse(0)
    val profileImageUrl = convertToInt(user.profileImageUrl.getOrElse("0"))
    val age = user.age.getOrElse(0)
    val postScore = post.score.getOrElse(0)
    val postViewCount = post.viewCount.getOrElse(0)
    val commentCount = post.commentCount.getOrElse(0)



    val userId: Int = elem.user.get.id.get
    val sid : Int = elem.key.get.split(":")(0).toInt
    val completionTime = 0D
    val nextUserId = 0

    val cellElement = Answer(

      reputation,
      creationDate,
      displayName,
      lastAccessDate,
      webSiteUrl,
      location,
      aboutMe,
      views,
      upVotes,
      downVotes,
      profileImageUrl,
      age,
      postScore,
      postViewCount,
      commentCount,
      nextUserId,
      completionTime,
      sid,
      answerId

    )
    val jValue = Extraction.decompose(cellElement)
    userId +" :: "+ compact(render(jValue))
  }

  private def getAnswer(elem: SequenceElem, nextElem: SequenceElem, timeLastItem: Double): String = {

    val user = elem.user.get
    val post =elem.post
    val answerId = post.id.getOrElse(0)
    val reputation = user.reputation.getOrElse(0)
    val creationDate = post.creationDate.get.getMillis
    val displayName = convertToInt(user.displayName.getOrElse("0"))
    val lastAccessDate = user.lastAccessDate.getOrElse(DateTime.now()).getMillis
    val webSiteUrl = convertToInt(user.websiteUrl.getOrElse("0"))
    val location = convertToInt(user.location.getOrElse("0"))
    val aboutMe = convertToInt(user.aboutMe.getOrElse("0"))
    val views = user.views.getOrElse(0)
    val upVotes = user.upVotes.getOrElse(0)
    val downVotes = user.downVotes.getOrElse(0)
    val profileImageUrl = convertToInt(user.profileImageUrl.getOrElse("0"))
    val age = user.age.getOrElse(0)
    val postScore = post.score.getOrElse(0)
    val postViewCount = post.viewCount.getOrElse(0)
    val commentCount = post.commentCount.getOrElse(0)



    val userId: Int = elem.user.get.id.get
    val sid : Int = elem.key.get.split(":")(0).toInt
    val completionTime = timeLastItem - creationDate
    val nextUserId = nextElem.user.get.id.get

    val cellElement = Answer(
      reputation,
      creationDate,
      displayName,
      lastAccessDate,
      webSiteUrl,
      location,
      aboutMe,
      views,
      upVotes,
      downVotes,
      profileImageUrl,
      age,
      postScore,
      postViewCount,
      commentCount,
      nextUserId,
      completionTime,
      sid,
      answerId
    )
    val jValue = Extraction.decompose(cellElement)
    userId +" :: "+ compact(render(jValue))
  }
  private def convertToInt(x: String): Int =
    if (x == "0") 0
    else 1
  /**
   *
   * @param minSize
   * @return a sequence in the form tid :: data where data = CellElement
   */
  def generateSequenceDataset(minSize: Int)(implicit sc: SparkContext): RDD[String] = {


    val formats = DefaultFormats

    val elements = sc.
      hbaseTable[SequenceElem](sequenceTableName).
      inColumnFamily("post,user")

    val groupedQuestions = elements.groupBy {
      case SequenceElem(id, post, user) =>
        id.get.split(":")(0)
    }

    val sequences = groupedQuestions.map {
      case (sequenceId, elements) =>
        //sort and remove the head
        //elimino quegli elements per cui non esiste creation date e
        elements.toList.filter(_.user.isDefined).filter(_.post.creationDate.isDefined).filter(_.user.get.id.size > 0).sortBy(_.post.id.get)
    }

    sequences.filter(_.size > minSize).map { seq =>

      val sid = seq.head.post.id.get
      val lastElement = seq.last
      val timeLastElement= lastElement.post.creationDate.get.getMillis
      val firstAnswers = for{
        i<-1 until seq.size-1
      } yield  getAnswer(seq(i), seq(i+1), timeLastElement)

      //add last answer
      val answers = firstAnswers.toList :+ getLastCellElement(lastElement)

      answers.mkString(" -1 ") + " -1 -2"
    }
  }

  val categoricalFeatures = Map(2 -> 2, 4 -> 2, 5 -> 2, 6 ->2, 10 -> 2)

 // val ignoreFeatures = List(0,1,)

  val numFeatures = 19
}

object MainDataset extends App with Serializable {

  args match {
    case Array(usersPath, postsPath) =>

    //val usersPath = "src/test/resources/Users.xml"
    //val postsPath = "src/test/resources/Posts.xml"

    val sparkConf = new SparkConf()
    sparkConf.set("spark.hbase.host", "localhost")
    implicit val sc = new SparkContext("local", "Main", sparkConf)
    StackExchange.init(usersPath, postsPath)

    val dataset = StackExchangeDataset.generateSequenceDataset(2)
    dataset.foreach(println)

    sc.stop()

    case _ => println("Insert users path and posts path")
  }
}

