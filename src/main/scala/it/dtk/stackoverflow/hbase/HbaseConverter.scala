package it.dtk.stackoverflow.hbase

import it.dtk.stackoverflow.hbase.ModelDAO.PostDAO
import it.nerdammer.spark.hbase.conversion._
import it.dtk.stackoverflow.Model._
import org.apache.hadoop.hbase.util.Bytes
import org.joda.time.DateTime
import scala.language.implicitConversions
import scala.util.{Failure, Success, Try}

/**
 * Created by fabiofumarola on 18/02/15.
 */
object HbaseConverter extends Serializable {

  implicit def intToBytes(x: Option[Int]): Option[Array[Byte]] =
    x.map(Bytes.toBytes)

  implicit def stringToBytes(x: Option[String]) =
    x.map(Bytes.toBytes)

  implicit def datetimeToBytes(x: Option[DateTime]) =
    x.map(d => Bytes.toBytes(d.getMillis))

  implicit def userWriter: FieldWriter[User] = new FieldWriter[User] {

    override def map(user: User): HBaseData = {

      val key = user.id.map(i => Bytes.toBytes(i.toString))

      Seq[Option[Array[Byte]]](
        key,
        user.id,
        user.creationDate,
        user.displayName,
        user.lastAccessDate,
        user.websiteUrl,
        user.location,
        user.aboutMe,
        user.views,
        user.upVotes,
        user.downVotes,
        user.profileImageUrl,
        user.age,
        user.accountId
      )
    }

    override def columns =
      Seq("id", "creationDate", "displayName", "lastAccessDate", "websiteUrl", "location", "aboutMe",
        "views", "upVotes", "downVotes", "profileImageUrl", "age", "accountId")
  }

  implicit def userReader: FieldReader[User] = new FieldReader[User] {

    override def map(data: HBaseData): User =
      User(
        id = data.drop(1).head.map(Bytes.toInt),
        creationDate = data.drop(2).head.map(v => new DateTime(Bytes.toLong(v))),
        displayName = data.drop(3).head.map(Bytes.toString),
        lastAccessDate = data.drop(4).head.map(v => new DateTime(Bytes.toLong(v))),
        websiteUrl = data.drop(5).head.map(Bytes.toString),
        location = data.drop(6).head.map(Bytes.toString),
        aboutMe = data.drop(7).head.map(Bytes.toString),
        views = data.drop(8).head.map(Bytes.toInt),
        upVotes = data.drop(9).head.map(Bytes.toInt),
        downVotes = data.drop(10).head.map(Bytes.toInt),
        profileImageUrl = data.drop(11).head.map(Bytes.toString),
        age = data.drop(12).head.map(Bytes.toInt),
        accountId = data.drop(13).head.map(Bytes.toInt)
      )

    override def columns =
      Seq("id", "creationDate", "displayName", "lastAccessDate", "websiteUrl", "location", "aboutMe",
        "views", "upVotes", "downVotes", "profileImageUrl", "age", "accountId")
  }

  implicit def postWriter: FieldWriter[Post] = new FieldWriter[Post] {

    override def map(data: Post): HBaseData = {

      val key = PostDAO.generateKey(data)

      Seq[Option[Array[Byte]]](
        key,
        data.id,
        data.postTypeId,
        data.parentId,
        data.acceptedAnswerId,
        data.creationDate,
        data.score,
        data.viewCount,
        data.body,
        data.ownerUserId,
        data.lastEditorUserId,
        data.lastEditDate,
        data.lastActivityDate,
        data.commentCount
      )
    }

    override def columns =
      Seq("id", "postTypeId", "parentId", "acceptedAnswerId", "creationDate", "score",
        "viewCount", "body", "ownerUserId", "lastEditorUserId", "lastEditDate", "lastActivityDate", "commentCount")
  }

  implicit def postReader = new FieldReader[Post] {

    override def map(data: HBaseData): Post =
      Post(
        id = data.drop(1).head.map(Bytes.toInt),
        postTypeId = data.drop(2).head.map(Bytes.toInt),
        parentId = data.drop(3).head.map(Bytes.toInt),
        acceptedAnswerId = data.drop(4).head.map(Bytes.toInt),
        creationDate = data.drop(5).head.map(v => new DateTime(Bytes.toLong(v))),
        score = data.drop(6).head.map(Bytes.toInt),
        viewCount = data.drop(7).head.map(Bytes.toInt),
        body = data.drop(8).head.map(Bytes.toString),
        ownerUserId = data.drop(9).head.map(Bytes.toInt),
        lastEditorUserId = data.drop(10).head.map(Bytes.toInt),
        lastEditDate = data.drop(11).head.map(v => new DateTime(Bytes.toLong(v))),
        lastActivityDate = data.drop(12).head.map(v => new DateTime(Bytes.toLong(v))),
        commentCount = data.drop(13).head.map(Bytes.toInt)
      )

    override def columns =
      Seq("id", "postTypeId", "parentId", "acceptedAnswerId", "creationDate", "score",
        "viewCount", "body", "ownerUserId", "lastEditorUserId", "lastEditDate", "lastActivityDate", "commentCount")
  }

  implicit def sequenceElemWriter: FieldWriter[SequenceElem] = new FieldWriter[SequenceElem] {

    override def map(data: SequenceElem): HBaseData = {
      //tail to remove the key that we do not need to persist another time
      val user = data.user.getOrElse(User(Option(-1)))
      Seq[Option[Array[Byte]]](data.key) ++ postWriter.map(data.post).tail ++ userWriter.map(user).tail
    }

    val userInfos = Map("user" -> List("id", "creationDate", "displayName", "lastAccessDate", "websiteUrl", "location", "aboutMe",
      "views", "upVotes", "downVotes", "profileImageUrl", "age", "accountId"))

    val postInfos = Map("post" -> List("id", "postTypeId", "parentId", "acceptedAnswerId", "creationDate", "score",
      "viewCount", "body", "ownerUserId", "lastEditorUserId", "lastEditDate", "lastActivityDate", "commentCount"))

    override def columns = {
      val cols = postInfos.flatMap(kv => kv._2.map(e => s"${kv._1}:$e")) ++ userInfos.flatMap(kv => kv._2.map(e => s"${kv._1}:$e"))
      cols
    }

  }

  implicit def sequenceElemReader = new FieldReader[SequenceElem] {

    override def map(data: HBaseData): SequenceElem = {

      val result = Try {
        SequenceElem(
          key = data.head.map(Bytes.toString),
          post = postReader.map(data),
          user = Option(userReader.map(data.drop(13)))
        )
      }

      result match {
        case Success(seq) =>
          seq
        case Failure(ex) =>
          SequenceElem(None, Post() ,None)
      }
    }

    val userInfos = Map("user" -> List("id", "creationDate", "displayName", "lastAccessDate", "websiteUrl", "location", "aboutMe",
      "views", "upVotes", "downVotes", "profileImageUrl", "age", "accountId"))

    val postInfos = Map("post" -> List("id", "postTypeId", "parentId", "acceptedAnswerId", "creationDate", "score",
      "viewCount", "body", "ownerUserId", "lastEditorUserId", "lastEditDate", "lastActivityDate", "commentCount"))

    override def columns = {
      val cols = postInfos.flatMap(kv => kv._2.map(e => s"${kv._1}:$e")) ++ userInfos.flatMap(kv => kv._2.map(e => s"${kv._1}:$e"))
      cols
    }
  }

}

