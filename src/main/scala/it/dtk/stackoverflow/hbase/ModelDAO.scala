package it.dtk.stackoverflow.hbase

import org.apache.spark.SparkContext
import it.dtk.stackoverflow.Model._
import it.nerdammer.spark.hbase._
import HbaseConverter._

/**
 * Created by fabiofumarola on 20/02/15.
 */
object ModelDAO extends Serializable {

  def pad(str: String, size: Int): String =
    if(str.length>=size) str
    else pad("0" + str, size)

  object UserDAO {

    //FixMe
    def findUser(key: String)(implicit sc: SparkContext): Option[User] = {
      val query = sc.hbaseTable[User]("users")
        .inColumnFamily("info")
        .withStartRow(key)
        .withStopRow(key)

      query.collect().headOption
    }
  }

  object PostDAO {

    def generateKey(post: Post): Option[String] =
      post.postTypeId.map { t =>
        if (t == 1)
          s"${post.id.get}:0"
        else s"${post.parentId.get}:${post.id.get}"

      }

  }

  object SequenceDAO {

    def getSequenceElement(rowKey: String): Answer =
      HBaseAdminUtils.getCellElem(rowKey)
  }

}
