package it.kdde.experiments

import it.kdde.distributed.preprocessing.{BPI2013Preprocessor, XESPreprocessor}
import it.kdde.distributed.sequentialpatterns.{DFast, PmmWithFast, PmmWithFastModel}
import it.kdde.distributed.util.Util
import it.kdde.distributed.util.Util.Result
import it.kdde.util.Statistics
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer
import scala.util.Properties

/**
  * This class is used to test various parameters of the pmmWithFast algorithm with the BPI Challenge 2013 dataset.
  * It can use 70/30 split or Cross-Validation to evaluate the performance.
  * The best set of parameters is the one that achieves the maximum classification accuracy.
  */
object BPI2013TuningMain extends App with Serializable {

  args match {
    case Array(algorithm, splitType, inputFile, numPartitions, minSupp, confidence, maxDepthEvaluation,  statisticsFile) =>

      val master = Properties.envOrElse("MASTER", "local")
      val conf = new SparkConf().setAppName("PmmWithFastBPI2015")
        .set("spark.executor.memory", "2g")
        .set("spark.driver.memory", "2g")
        .set("spark.serialization","org.apache.spark.serializer.KyroSerializer")
        .set("spark.io.compression.codec","org.apache.spark.io.SnappyCompressionCodec")
      implicit val sc = new SparkContext(master, "PmmWithFast", conf)
      sc.setLogLevel("ERROR")

      val algorithmType = DFast.AssociationAlgorithm.fromString(algorithm)
      val lines = sc.textFile(inputFile)
      //Load the dataset
      val dataset = sc.textFile(inputFile, numPartitions.toInt)

      var splits: Array[RDD[String]] = null
      var trainingData: RDD[String] = null
      var testingData: RDD[String] = null

      splitType match {
        case "simple" => {
          //Split the dataset in training and test set
          splits = dataset.randomSplit(Array(0.7, 0.3))
          trainingData = splits(0)
          testingData = splits(1)
        }
        case "crossValidation" => {
          splits = dataset.randomSplit(Array(0.2, 0.2, 0.2, 0.2, 0.2))
        }
      }


      val statistics = new Statistics

      BPI2013Preprocessor.path = inputFile
      BPI2013Preprocessor.initialize()

      val minSupps = Array(0.2, 0.3, 0.4)
      val types = Array("frequent", "contiguous", "opening")


      //Evaluation settings, each element is a couple (algorithmType, minSupp)
      val settings = Array(
        (0, 0), (0, 1), (0, 2),
        (1, 0), (1, 1), (1, 2),
        (2, 0), (2, 1), (2, 2)
      )

      var models = new ArrayBuffer[PmmWithFastModel]
      var accuracies = new ArrayBuffer[Double]
      //var resObjects = new ArrayBuffer[ArrayBuffer[RDD[Result]]]
      var times = new ArrayBuffer[Long]
      var bestIndex = -1
      var bestTime: Long = 0

      for (i <- settings.indices) {
        var test: ParamResult = null
        var currentParamAccuracy = 0.0

        //var resFolds = new ArrayBuffer[RDD[Result]]
        splitType match {
          case "crossValidation" => {
            var foldAccSum = 0.0
            for (testFold <- splits.indices) {
              //For each fold, train the model
              println("calculating fold " + testFold)
              val testSet = splits(testFold)
              val trainSet = getTrainingFolds(sc, splits, testFold)

              test = evaluateParam(
                DFast.AssociationAlgorithm.fromString(types(settings(i)._1)),
                confidence,
                trainSet,
                testSet,
                minSupps(settings(i)._2).toFloat,
                statistics
              )
              //Save the stats to a file
              Util.printStat(inputFile, numPartitions, minSupps(settings(i)._2) + "", confidence, test.results, test.time, test.model, statisticsFile + "_setting" + i + "_fold" + testFold)

              //resFolds += test.results
              println("fold accuracy is " + test.accuracy)
              foldAccSum += test.accuracy
            }
            //Calculate this setting's accuracy as an average of the accuracies for each run
            currentParamAccuracy = foldAccSum / splits.length
          }
          case "simple" => {
            test = evaluateParam(
              DFast.AssociationAlgorithm.fromString(types(settings(i)._1)),
              confidence,
              trainingData,
              testingData,
              minSupps(settings(i)._2).toFloat,
              statistics
            )

            //resFolds += test.results
            currentParamAccuracy = test.accuracy
          }
        }
        accuracies += currentParamAccuracy

        //Find the best setting yet
        if (bestIndex == -1) {
          bestIndex = i
        } else {
          if (currentParamAccuracy > accuracies(bestIndex)) {
            bestIndex = i
          }
        }
      }

      //Print the result
      for(i <- settings.indices) {
        println("setting is " + i + ", params are: minSup=" + minSupps(settings(i)._2) + ", algorithmType=" + types(settings(i)._1))
        println("accuracy is " + accuracies(i))
      }

      println("best setting is " + settings(bestIndex))
      /*var count = 0
      for (resObject <- resObjects(bestIndex)) {
        Util.printStat(inputFile, numPartitions, minSupps(settings(bestIndex)._2) + "", confidence, resObject, times(bestIndex), models(bestIndex), statisticsFile + "_fold" + count)
        count += 1
      }*/
      sc.stop()

      //sc.parallelize(pmmWithFastModel.mapClassifiers.toList).saveAsTextFile("/home/lanotte/classifiers.txt")
      //sc.parallelize(pmmWithFastModel.mapRegressors.toList).saveAsTextFile("/home/lanotte/regressors.txt")
      //sc.parallelize(pmmWithFastModel.patterns).saveAsTextFile("/home/lanotte/patterns.txt")

      //Util.printStat(inputFile, numPartitions, minSupp, confidence, results, time, pmmWithFastModel, statisticsFile)

      sc.stop()

    case a =>

      println(
        s"""
           |Insert
           | - Algorithm Type:
           |   - "frequent"
           |   - "opening"
           |   - "contiguous"
           | - inputFile path
           | - num. partitions (e.g. 2)
           | - min. support (e.g. 0.01)
           | - confidence (e.g. 0.95)
           | - max depth evaluation for test sequences
           | - statistics file absolute path
           |
          |   all the parameters should be defined in a single line with spaces
           |
          |   for  $a
        """.stripMargin)
  }

  def getTrainingFolds(sc: SparkContext, folds: Array[RDD[String]], testFold: Int): RDD[String] = {
    var list = new ArrayBuffer[RDD[String]]
    for (i <- folds.indices) {
      if (i != testFold) {
        list += folds(i)
      }
    }
    sc.union(list)
  }

  /**
    * Trains a model with pmmWithFast with the specified training set, test set, and parameters.
    * Returns training time, the classification accuracy, the resulting model and all the testing results
    */
  def evaluateParam(algorithmType: DFast.AssociationAlgorithm.Value,
                    confidence: String,
                    trainingData: RDD[String],
                    testingData: RDD[String],
                    minSup: Float,
                    statistics: Statistics): ParamResult = {
    println("Start training process")
    statistics.startTimeSequence

    //Train using PmmWithFast
    val pmmWithFastModel = PmmWithFast.train(
      associationAlgo = algorithmType,
      minSupp = minSup,
      confidence = confidence.toFloat,
      categoricalFeaturesInfo = BPI2013Preprocessor.categoricalFeatures,
      input = trainingData,
      BPI2013Preprocessor
    )
    statistics.endTimeSequence

    val time = statistics.sequenceTime()
    /*
    val predictions = pmmWithFastModel.testEnsemble(testingData, maxDepthEvaluation.toInt)
    val results = PmmWithFast.cumulativePerformance(predictions)
    //val results = PmmWithFast.computePerformance2(predictions)
    results.sortBy(_.sizeSeq).saveAsTextFile(statisticsFile)
    */
    //Test
    val predictions = pmmWithFastModel.test(testingData).cache()
    val results = PmmWithFast.computePerformance(predictions)

    var avg = 0.0
    var numSeq = 0
    for(res <- results.collect()) {
      avg += res.accuracy * res.numSeq
      numSeq += res.numSeq
    }
    val accuracy = avg / numSeq

    new ParamResult(time, accuracy, pmmWithFastModel, results)
  }

  class ParamResult(val time: Long, val accuracy: Double, val model: PmmWithFastModel, val results: RDD[Util.Result])

}
