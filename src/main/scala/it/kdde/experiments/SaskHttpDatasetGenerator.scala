package it.kdde.experiments

import java.io.{BufferedWriter, File, FileWriter}

import it.kdde.distributed.preprocessing._
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Properties

/**
 * Created by fabiana on 3/26/15.
 */
object SaskHttpDatasetGenerator extends App {
  args match {
    case Array(inputFile, minLength, outPutFile) =>
      val master = Properties.envOrElse("MASTER", "local")
      //Using local as the default value for the master machine makes it easy to launch your application locally in a test environment.
      val conf = new SparkConf().setAppName("SaskHttpDatasetGenerator")
      implicit val sc = new SparkContext(master, "SaskHttpDatasetGenerator", conf)

      val lines = sc.textFile(inputFile)
      val dataset = SaskHttpPreprocessor.generateSequenceDataset(lines, minLength.toInt)

      //Codice Iovine
      val l = dataset.collect()
      val file = new File(outPutFile)
      val bw = new BufferedWriter(new FileWriter(file))
      l.foreach{ s =>
        {
          bw.write(s + "\n")
        }
      }
      bw.close()
      //Codice Iovine

      //dataset.saveAsTextFile(outPutFile)

    case _ => println("Insert inputFile, the minimum length of the sequences, and outputFile")
  }


}
