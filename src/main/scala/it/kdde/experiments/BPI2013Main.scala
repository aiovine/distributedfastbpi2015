package it.kdde.experiments

import it.kdde.distributed.preprocessing.{BPI2013Preprocessor, XESPreprocessor}
import it.kdde.distributed.sequentialpatterns.{DFast, PmmWithFast}
import it.kdde.distributed.util.Util
import it.kdde.util.Statistics
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Properties

/**
  * Created by fabiana on 3/24/15.
  */
object BPI2013Main extends App with Serializable {

  args match {
    case Array(algorithm, inputFile, numPartitions, minSupp, confidence, maxDepthEvaluation,  statisticsFile) =>

      val master = Properties.envOrElse("MASTER", "local")
      val conf = new SparkConf().setAppName("PmmWithFastBPI2015")
        .set("spark.executor.memory", "2g")
        .set("spark.driver.memory", "2g")
        .set("spark.serialization","org.apache.spark.serializer.KyroSerializer")
        .set("spark.io.compression.codec","org.apache.spark.io.SnappyCompressionCodec")
      implicit val sc = new SparkContext(master, "PmmWithFast", conf)

      val algorithmType = DFast.AssociationAlgorithm.fromString(algorithm)
      val lines = sc.textFile(inputFile)
      //Load the dataset
      val dataset = sc.textFile(inputFile, numPartitions.toInt)

      //Split the dataset in training and test set
      val splits = dataset.randomSplit(Array(0.7, 0.3))
      val trainingData = splits(0)
      val testingData = splits(1)

      val statistics = new Statistics

      BPI2013Preprocessor.path = inputFile
      BPI2013Preprocessor.initialize()

      println("Start training process")
      statistics.startTimeSequence

      //Train using PmmWithFast
      val pmmWithFastModel = PmmWithFast.train(
        associationAlgo = algorithmType,
        minSupp = minSupp.toFloat,
        confidence = confidence.toFloat,
        categoricalFeaturesInfo = BPI2013Preprocessor.categoricalFeatures,
        input = trainingData,
        BPI2013Preprocessor
      )
      statistics.endTimeSequence

      val time = statistics.sequenceTime()
      /*
      val predictions = pmmWithFastModel.testEnsemble(testingData, maxDepthEvaluation.toInt)
      val results = PmmWithFast.cumulativePerformance(predictions)
      //val results = PmmWithFast.computePerformance2(predictions)
      results.sortBy(_.sizeSeq).saveAsTextFile(statisticsFile)
      */
      //Test
      val predictions = pmmWithFastModel.test(testingData).cache()
      val results = PmmWithFast.computePerformanceWithRRMSE(predictions)
      println("Start testing process")

      Util.printStatWithRRMSE(inputFile, numPartitions, minSupp, confidence, results, time, pmmWithFastModel, statisticsFile)
      sc.stop()

      //sc.parallelize(pmmWithFastModel.mapClassifiers.toList).saveAsTextFile("/home/lanotte/classifiers.txt")
      //sc.parallelize(pmmWithFastModel.mapRegressors.toList).saveAsTextFile("/home/lanotte/regressors.txt")
      //sc.parallelize(pmmWithFastModel.patterns).saveAsTextFile("/home/lanotte/patterns.txt")

      //Util.printStat(inputFile, numPartitions, minSupp, confidence, results, time, pmmWithFastModel, statisticsFile)

      sc.stop()

    case a =>

      println(
        s"""
           |Insert
           | - Algorithm Type:
           |   - "frequent"
           |   - "opening"
           |   - "contiguous"
           | - inputFile path
           | - num. partitions (e.g. 2)
           | - min. support (e.g. 0.01)
           | - confidence (e.g. 0.95)
           | - max depth evaluation for test sequences
           | - statistics file absolute path
           |
          |   all the parameters should be defined in a single line with spaces
           |
          |   for  $a
        """.stripMargin)
  }




}

/*package it.kdde.experiments

import it.kdde.distributed.preprocessing.{SaskHttpPreprocessor, XESPreprocessor}
import it.kdde.distributed.sequentialpatterns.{DFast, PmmWithFast}
import it.kdde.util.Statistics
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Properties

object BPI2015Main extends App with Serializable {

  args match {
    case Array(algorithm, inputFile, numPartitions, minSupp, confidence, maxDepthEvaluation,  statisticsFile) =>

      val master = Properties.envOrElse("MASTER", "local")
      val conf = new SparkConf().setAppName("PmmWithFastSask")
        .set("spark.executor.memory", "512m")
        .set("spark.driver.memory", "2g")
        .set("spark.yarn.executor.memoryOverhead", "1g")

      implicit val sc = new SparkContext(master, "PmmWithFast", conf)

      val algorithmType = DFast.AssociationAlgorithm.fromString(algorithm)
      val lines = sc.textFile(inputFile)
      val dataset = sc.textFile(inputFile, numPartitions.toInt)

      //dataset.persist(StorageLevel.DISK_ONLY)

      val splits = dataset.randomSplit(Array(0.7, 0.3))
      val trainingData = splits(0)
      val testingData = splits(1)
      //trainingData.persist(StorageLevel.DISK_ONLY)
      //testingData.persist(StorageLevel.DISK_ONLY)

      val statistics = new Statistics

      println("Start training process")
      statistics.startTimeSequence

      val pmmWithFastModel = PmmWithFast.train(
        associationAlgo = algorithmType,
        minSupp = minSupp.toFloat,
        confidence = confidence.toFloat,
        categoricalFeaturesInfo = SaskHttpPreprocessor.categoricalFeatures,
        input = trainingData,
        XESPreprocessor
      )
      statistics.endTimeSequence

      val time = statistics.sequenceTime()
      val predictions = pmmWithFastModel.testEnsemble(testingData, maxDepthEvaluation.toInt)
      val results = PmmWithFast.cumulativePerformance(predictions)
      //val results = PmmWithFast.computePerformance2(predictions)
      results.sortBy(_.sizeSeq).saveAsTextFile(statisticsFile)

      //sc.parallelize(pmmWithFastModel.mapClassifiers.toList).saveAsTextFile("/home/lanotte/classifiers.txt")
      //sc.parallelize(pmmWithFastModel.mapRegressors.toList).saveAsTextFile("/home/lanotte/regressors.txt")
      //sc.parallelize(pmmWithFastModel.patterns).saveAsTextFile("/home/lanotte/patterns.txt")

      //Util.printStat(inputFile, numPartitions, minSupp, confidence, results, time, pmmWithFastModel, statisticsFile)

      sc.stop()

    case a =>

      println(
        s"""
          |Insert
          | - Algorithm Type:
          |   - "frequent"
          |   - "opening"
          |   - "contiguous"
          | - inputFile path
          | - num. partitions (e.g. 2)
          | - min. support (e.g. 0.01)
          | - confidence (e.g. 0.95)
          | - max depth evaluation for test sequences
          | - statistics file absolute path
          |
          |   all the parameters should be defined in a single line with spaces
          |
          |   for  $a
        """.stripMargin)
  }




}
*/