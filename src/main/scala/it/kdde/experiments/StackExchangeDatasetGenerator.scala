package it.kdde.experiments

import it.dtk.stackoverflow.StackExchange
import it.kdde.distributed.preprocessing._
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Properties

/**
 * Created by fabiana on 3/26/15.
 */
object StackExchangeDatasetGenerator extends App {
  args match {
    case Array(minLength, outPutFile, usersPath, postsPath) =>
      val master = Properties.envOrElse("MASTER", "local")
      //Using local as the default value for the master machine makes it easy to launch your application locally in a test environment.
      val conf = new SparkConf().setAppName("StackExchangeDatasetGenerator")
      implicit val sc = new SparkContext(master, "StackExchangeDatasetGenerator", conf)


      StackExchange.init(usersPath, postsPath)
      val preprocessor: Preprocessor = StackExchangePreprocessor
      val dataset = preprocessor.generateSequenceDataset(null, minLength.toInt)
      dataset.saveAsTextFile(outPutFile)

    case _ => println("Insert the minimum length of the sequences, outputFile, path users and path posts")
  }
}
