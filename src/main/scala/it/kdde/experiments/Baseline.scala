package it.kdde.experiments

import java.nio.file.{ Path, Paths }
import java.util.Calendar

import it.kdde.distributed.preprocessing.{ Preprocessor, SaskHttpPreprocessor, StackExchangePreprocessor }
import it.kdde.distributed.sequentialpatterns.Dataset
import it.kdde.util.Statistics
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.apache.spark.rdd.RDD
import org.apache.spark.{ SparkConf, SparkContext }
import org.json4s.DefaultFormats

import scala.util.Properties

/**
 * Created by fabiana on 3/19/15.
 */
class Baseline(val preprocessor: Preprocessor)(implicit sc: SparkContext) extends Serializable {

  val impurity = "gini"
  val impurityRegression = "variance"
  val maxDepth = 5
  val maxBins = 32
  var labelsMap: Map[Double, Double] = Map()

  def mapLabels(dataset: RDD[String]): Map[Double, Double] = {
    val labels = dataset.map { sequence =>
      preprocessor.getLabelClassification(sequence.split(Dataset.ITEM_SEPARATOR)(1))
    }

    val distinctLabel = labels.distinct().collect()
    val indexList = for (i <- 0 until distinctLabel.size) yield i.toDouble
    val map = distinctLabel.zip(indexList).toMap
    map
  }

  /**
   *
   * @param dataset contains sequences in the form tid :: data -1 tid :: data -1 ... -1 -2
   * @return decision tree model learned from dataset obtained from input dataset, splitting all sequences in sequences of size 1  tid :: data -1 -2
   */
  def transformDataset(dataset: RDD[String]): RDD[String] = {
    implicit val formats = DefaultFormats

    val trainingDataset = dataset.flatMap { sequence =>

      //removed all sequences with nextItem = 0 and completionTime=0
      val items = sequence.split(Dataset.ITEMSET_SEPARATOR).dropRight(2)
      items.map { item =>
        item.trim + " -1 -2"
      }
    }
    labelsMap = mapLabels(trainingDataset)
    trainingDataset
  }

  def trainClassification(trainingDataset: RDD[String]): DecisionTreeModel = {
    val classificationDataset = trainingDataset.map { sequence =>
      val vector = preprocessor.extractFeatureSequence(sequence)
      val json = sequence.split(Dataset.ITEMSET_SEPARATOR)(0).split(Dataset.ITEM_SEPARATOR)(1)
      val label = preprocessor.getLabelClassification(json)
      LabeledPoint(labelsMap.get(label).get, vector)
    }

    val classes = labelsMap.keys.size
    DecisionTree.trainClassifier(classificationDataset, classes, preprocessor.categoricalFeatures, impurity, maxDepth, maxBins)
  }

  def trainRegression(trainingDataset: RDD[String]): DecisionTreeModel = {
    val classificationDataset = trainingDataset.map { sequence =>
      val vector = preprocessor.extractFeatureSequence(sequence)
      val json = sequence.split(Dataset.ITEMSET_SEPARATOR)(0).split(Dataset.ITEM_SEPARATOR)(1)
      val label = preprocessor.getLabelRegression(json)
      LabeledPoint(label, vector)
    }
    val model: DecisionTreeModel = DecisionTree.trainRegressor(classificationDataset, preprocessor.categoricalFeatures, impurityRegression, maxDepth, maxBins)
    model
  }

  def testRegression(testingDataset: RDD[String], model: DecisionTreeModel): RDD[(Double, Double)] = {
    val labeledPoints = testingDataset.map { sequence =>
      val vector = preprocessor.extractFeatureSequence(sequence)
      val json = sequence.split(Dataset.ITEMSET_SEPARATOR)(0).split(Dataset.ITEM_SEPARATOR)(1)
      val label = preprocessor.getLabelRegression(json)
      LabeledPoint(label, vector)
    }
    labeledPoints.map { point =>
      val prediction = model.predict(point.features)
      (point.label, prediction)
    }
  }

  def testClassification(testingDataset: RDD[String], model: DecisionTreeModel): RDD[(Double, Double)] = {
    val labeledPoints = testingDataset.map { sequence =>
      val vector = preprocessor.extractFeatureSequence(sequence)
      val json = sequence.split(Dataset.ITEMSET_SEPARATOR)(0).split(Dataset.ITEM_SEPARATOR)(1)
      val label = preprocessor.getLabelClassification(json)
      val mapLabel = labelsMap.get(label).get
      LabeledPoint(mapLabel, vector)
    }

    labeledPoints.map { point =>
      val prediction = model.predict(point.features)
      (point.label, prediction)
    }

  }

  def getAccuracyModel(labelAndPreds: RDD[(Double, Double)], sizeTestData: Long): Double = {
    labelAndPreds.filter(r => r._1 != r._2).count.toDouble / sizeTestData
  }

  def getRMSEModel(labelAndPreds: RDD[(Double, Double)]): Double = {
    val MSE = labelAndPreds.map { case (v, p) => math.pow((v - p), 2) }.reduce(_ + _) / labelAndPreds.count()
    Math.abs(MSE)
  }
}

object Baseline extends App {
  args match {
    case Array(inputFile, numPartitions, configuration, statisticsFile) =>

      val master = Properties.envOrElse("MASTER", "local")
      //Using local as the default value for the master machine makes it easy to launch your application locally in a test environment.
      val conf = new SparkConf().setAppName("Baseline")
      implicit val sc = new SparkContext(master, "Baseline", conf)

      val preprocessor = configuration match {
        case one if (one == 1) => StackExchangePreprocessor
        case _                 => SaskHttpPreprocessor
      }

      val baseline = new Baseline(preprocessor)
      val statistics = new Statistics
      val originalDataset = sc.textFile(inputFile, numPartitions.toInt)
      //val originalDataset = preprocessor.generateSequenceDataset(null, 2)

      val trasformedDataset = baseline.transformDataset(originalDataset)
      val splits = trasformedDataset.randomSplit(Array(0.7, 0.3))
      val trainingData = splits(0)
      val testingData = splits(1)

      println("Start classification process")
      statistics.startTimeSequence
      val modelClassificator = baseline.trainClassification(trainingData)
      println("End classification process")

      println("Start regression process")
      val modelRegressor = baseline.trainRegression(trainingData)
      println("End classification process")

      val resultsClassification = baseline.testClassification(testingData, modelClassificator)
      val resultsRegression = baseline.testRegression(testingData, modelRegressor)

      val accuracy = baseline.getAccuracyModel(resultsClassification, testingData.count())
      val rmse = baseline.getRMSEModel(resultsRegression)

      modelClassificator.save(sc, statisticsFile)
      println("Classifier\n" + modelClassificator)
      println("Regressor\n" + modelRegressor)

      //printStat(inputFile ,numPartitions ,rmse,accuracy,trainingData.count(),statistics.sequenceTime(),statisticsFile)

      println("Num. Seq. training: " + trainingData.count())
      println("Num. Seq. testing: " + testingData.count())
      println("Accuracy: " + accuracy)
      println("RMSE: " + rmse)
      sc.stop()
    case _ => println("Insert input file path, the number of partitions, configuration (if 1 run StackExchangePreprocessor otherwise SackHttpPreprocessor) and statisticsFile")

  }

  def roundAt(n: Double, p: Int): Double = { val s = math pow (10, p); (math round n * s) / s }

  def printStat(inputFile: String, numPartitions: String, rmse: Double, accuracy: Double, trainingSize: Long, time: Long, statisticsFile: String)(implicit sc: SparkContext): Unit = {
    val header = "Data in Millisec, InputFile, Partitions, num. Seq., RMSE, accuracy, time\n"
    val statsFile: Path = Paths.get(statisticsFile)

    val rmseRound = roundAt(rmse, 2)

    val accuracyRound = roundAt(accuracy, 2)

    val string = header + Calendar.getInstance().getTimeInMillis() + "," + inputFile + "," + numPartitions + "," + trainingSize + "," + rmseRound + "," + accuracyRound + "," + time + " \n"
    println(string)
    sc.parallelize(string).saveAsTextFile(statisticsFile)

  }

}
