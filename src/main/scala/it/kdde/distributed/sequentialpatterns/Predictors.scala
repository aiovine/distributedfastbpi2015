package it.kdde.distributed.sequentialpatterns

import it.kdde.distributed.preprocessing.Preprocessor
import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.apache.spark.rdd.{ PairRDDFunctions, RDD }

/**
 * Created by fabiofumarola on 26/02/15.
 */
object Predictors extends Serializable {

  /**
   *
   * @param patterns
   * @return
   */
  def toLabeledPointClassification(patterns: PairRDDFunctions[String, Seq[Seq[ListNode]]], preprocessor: Preprocessor): PairRDDFunctions[String, LabeledPoint] =
    convert(preprocessor.getLabeledPointForClassification)(patterns)

  def toLabeledPointRegression(patterns: PairRDDFunctions[String, Seq[Seq[ListNode]]], preprocessor: Preprocessor) =
    convert(preprocessor.getLabeledPointForRegression)(patterns)

  private def convert(f: Seq[ListNode] => LabeledPoint)(patterns: PairRDDFunctions[String, Seq[Seq[ListNode]]]): PairRDDFunctions[String, LabeledPoint] = {
    patterns.flatMapValues(rows => rows.map(f))
  }

  class Classifier(val trainingData: RDD[LabeledPoint],
                   val categoricalFeaturesInfo: Map[Int, Int],
                   val impurity: String = "gini",
                   val maxDepth: Int = 5,
                   val maxBins: Int = 1200) extends Serializable {

    val classes = trainingData.map(_.label).distinct().collect()

    val mapClassInt = {
      val positions = (0 until classes.length).toArray
      classes.zip(positions).toMap
    }

    val mapIntClass = {
      val positions = (0 until classes.length).toArray
      positions.zip(classes).toMap
    }

    val model =
      if (classes.size > 1) {
        val data = trainingData.map(lp => lp.copy(mapClassInt(lp.label)))
        Some(DecisionTree.trainClassifier(data, classes.length, categoricalFeaturesInfo, impurity, maxDepth, maxBins))
      }
      else None

    /**
     *
     * @param features
     * @return if there are more distinct classes return the class predicted, otherwise return the only class associate to the Classifier
     */
    def predict(features: Vector): Double = {
      if (model.isDefined) {
        val prediction = model.get.predict(features)
        mapIntClass(prediction.toInt)
      }
      else {
        classes(0)
      }
    }

  }

  class Regressor(val trainingData: RDD[LabeledPoint],
                  val categoricalFeaturesInfo: Map[Int, Int],
                  val impurity: String = "variance",
                  val maxDepth: Int = 5,
                  val maxBins: Int = 32) extends Serializable {

    val model = train

    def predict(features: Vector): Double =
      model.predict(features)

    private def train: DecisionTreeModel = {
      DecisionTree.trainRegressor(trainingData, categoricalFeaturesInfo, impurity, maxDepth, maxBins)
    }
  }

}
