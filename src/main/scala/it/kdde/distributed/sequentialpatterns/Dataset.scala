package it.kdde.distributed.sequentialpatterns

import it.kdde.TailInequalities
import it.kdde.sequentialpatterns.model.SparseIdList
import org.json4s.DefaultFormats

import scala.collection.immutable.TreeMap

/**
 * Created by fabiofumarola on 26/02/15.
 */
class Dataset(private[sequentialpatterns] var silsMap: TreeMap[String, SparseIdList],
              val numRows: Long,
              val minSupp: Float,
              val confidence: Float) extends Serializable {

  val epsilon = TailInequalities.chernoffBound(minSupp, confidence, numRows)

  //in case ob value lower than zero returns 1
  val absoluteMinSupp = Math.ceil((minSupp - epsilon) * numRows).toInt match {
    case value if value <= 0 => 1
    case other               => other
  }

  silsMap = silsMap.filter(_._2.getAbsoluteSupport >= absoluteMinSupp)

}

object Dataset extends Serializable {

  val ITEMSET_SEPARATOR: String = " -1 "
  val ITEM_SEPARATOR: String = " :: "
  val SEQUENCE_SEPARATOR: String = "-2"

  def load(dataset: List[String], minSupp: Float, confidence: Float): Dataset = {
    val formats = DefaultFormats
    var silsMap = TreeMap.empty[String, SparseIdList]

    var lineNumber = 0

    dataset.filter(_.length > 1).foreach { line =>

      //eliminati item con nextTid=0 e completionTime=0
      val sequence = line.split(ITEMSET_SEPARATOR).dropRight(2)

      val index = for (i <- 1 to sequence.size) yield i
      sequence.
        zip(index).
        map { items =>
          val itemData = items._1.split(ITEM_SEPARATOR)
          val item = itemData(0).trim
          val data: String = itemData(1).trim

          if (!silsMap.contains(item)) {
            val a = new SparseIdList(dataset.size)
            silsMap += item -> new SparseIdList(dataset.size)
          }
          silsMap(item).addElement(lineNumber, items._2, data)
        }
      lineNumber += 1
    }

    new Dataset(silsMap, dataset.size, minSupp, confidence)

  }

  /**
   *
   * @param UserSequence a sequence composed by only userId
   * @return number of users
   */
  def getNumItems(UserSequence: String) = UserSequence.split(Dataset.ITEMSET_SEPARATOR).size - 1

}
