package it.kdde.distributed.sequentialpatterns

import it.kdde.sequentialpatterns.fast.{Fast, FastDataset}
import it.kdde.sequentialpatterns.model.tree.SequenceNode
import it.kdde.sequentialpatterns.model.{ListNode, SparseIdList, VerticalIdList}

import scala.collection.JavaConversions._

/**
 * Created by fabiana on 1/28/15.
 */
object DistributedFast {

  def apply(fastDataset: Dataset): DistributedFast = {
    val df = new DistributedFast(fastDataset)
    df.run()
    df
  }
}

class DistributedFast(val fastDataset: Dataset) {

  private var frequentSequences: List[(String, Seq[Seq[ListNode]])] = List()
  private var fast: Fast = null


  private def mapSequenceForClassification(sequence: SequenceNode): (String, Seq[Seq[ListNode]]) = {

    var list = List[VerticalIdList]()
    var node = sequence

    while (node != fast.getTree.getRoot) {
      list = node.getVerticalIdList :: list
      node = node.getParent
    }

    val lastNodeElements = sequence.getVerticalIdList.getElements

    val rows = for (i <- 0 until lastNodeElements.size if (lastNodeElements(i) != null))
      yield for (j <- 0 until list.size) yield list(j).getElements()(i)

    (sequence.getSequence.toString, rows)
  }

  def run(): Unit = {
    println("Called DistributedFast.run") //Iovine
    println("Dataset has " + fastDataset.numRows + " rows") //Iovine
    val javaMap = fastDataset.silsMap.foldLeft(new java.util.TreeMap[String, SparseIdList]()) {
      case (acc, value) =>
        acc.put(value._1, value._2)
        acc
    }

    val ds = new FastDataset(javaMap, fastDataset.numRows, fastDataset.minSupp, fastDataset.absoluteMinSupp)
    val fast = new Fast(ds)
    fast.run()
    this.fast = fast
    this.frequentSequences =  fast.getFrequentSequences().toList.map(a => mapSequenceForClassification(a))
  }

  def getFrequentSequences() = this.frequentSequences

  def getTree() = this.fast.getTree

}
