package it.kdde.distributed.sequentialpatterns

import it.kdde.distributed.preprocessing.Preprocessor
import it.kdde.distributed.sequentialpatterns.DFast.AssociationAlgorithm
import it.kdde.distributed.sequentialpatterns.Predictors.{Classifier, Regressor}
import it.kdde.distributed.util.TreeUtils
import it.kdde.distributed.util.Util.{Prediction, Result, Result2}
import org.apache.spark.Logging
import org.apache.spark.annotation.Experimental
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD

import scala.collection.mutable

/**
 * Created by fabiofumarola on 27/02/15.
 */
@Experimental
class PmmWithFast(val associationAlgo: AssociationAlgorithm.Value,
                  val minSupp: Float,
                  val confidence: Float,
                  val categoricalFeaturesInfo: Map[Int, Int],
                  val preprocessor: Preprocessor) extends Serializable with Logging {
  /**
   *
   * @param sequence
   * @return
   */
  private def extractCategoricalFeatures(sequence: String): Map[Int, Int] = {

    categoricalFeaturesInfo.keys.flatMap { key =>

      val numberOfUsers = Dataset.getNumItems(sequence)
      val distinctValues = categoricalFeaturesInfo.get(key).get
      val usersIndexs = for (i <- 0 until numberOfUsers)
        yield (key + (i * preprocessor.numberFeatures))

      usersIndexs.map(index => (index, distinctValues))
    }.toMap
  }

  def run(input: RDD[String], preprocessor: Preprocessor): PmmWithFastModel = {
    println("Entered PmmWithFast.run") //Iovine
    log.debug("Start sequence generation process")
    //Get frequent/opening/contiguous sequences from Fast
    val patterns = DFast.train(input, associationAlgo, minSupp, confidence)
    log.debug("End sequence generation process")

    val count = patterns.count(); //Iovine

    println("Discovered " + count + " sequences") //Iovine
    log.info("discovered {} sequences", count)

    /* Iovine */
    val listPatterns = patterns.collect()
    println("start printing found sequences")
    for (x <- listPatterns) {
      println(x._1)
    }
    println("end printing found sequences")
    /*        */

    log.debug("Start classification")

    if(patterns.isEmpty()) throw new RuntimeException("No frequent sequences are returned, reduce the input support!")
    //From each discovered sequence get the attributes
    val classificationDatasets = Predictors.toLabeledPointClassification(patterns, preprocessor)
    //Group all the previously found rows by sequence
    val dsClassAggrBySeq = classificationDatasets.aggregateByKey(List[LabeledPoint]())((acc, value) => value :: acc, (v1, v2) => v1 ++ v2)

    /*    Codice Iovine
    println("Printing datasets for classification")
    val list = dsClassAggrBySeq.collect()

    for (l <- list) {
      println("sequence is " + l._1)
      val dataset = l._2
      for (d <- dataset) {
        println(d)
      }
    }
    ***********************/

      //For each sequence, learn a classifier
    val classifiers: Array[(String, Classifier)] = dsClassAggrBySeq.keys.collect().map { seq =>

      val rdd = dsClassAggrBySeq.context.parallelize(dsClassAggrBySeq.lookup(seq).flatMap(identity))
      val catFeatures = extractCategoricalFeatures(seq)
      val classifier = new Classifier(rdd, catFeatures)
      (seq, classifier)
    }
    log.debug("End classification")

    /* Codice Iovine */
    println("start printing classifiers")

    for (c <- classifiers) {
      println("sequence is " + c._1)
      //println("mapIntClass is" + c._2.mapIntClass)
      val classifier = c._2.model
      if (!classifier.isEmpty) {
        println(TreeUtils.printTree(classifier.get, c._2.mapIntClass))
      }
    }
    /*****************/

    log.debug("Start regression process")
    val dsRegr = Predictors.toLabeledPointRegression(patterns, preprocessor)
    val dsRegrAggrBySeq = dsRegr.aggregateByKey(List[LabeledPoint]())((acc, value) => value :: acc, (v1, v2) => v1 ++ v2)
    val regressors = dsRegrAggrBySeq.keys.collect().map { seq =>

      val rdd = dsRegrAggrBySeq.context.parallelize(dsRegrAggrBySeq.lookup(seq).flatMap(identity))
      val catFeatures = extractCategoricalFeatures(seq)
      val regressor = new Regressor(rdd, catFeatures)
      (seq, regressor)
    }
    log.debug("End regression process")

    /* Codice Iovine */
    println("start printing regressors")

    for (r <- regressors) {
      println("sequence is " + r._1)
      //println("mapIntClass is" + c._2.mapIntClass)
      val regressor = r._2.model
      println(TreeUtils.printTree(regressor, null))
    }
    /*****************/

    new PmmWithFastModel(patterns.keys.collect(), classifiers, regressors, preprocessor)
  }
}

class PmmWithFastModel(val patterns: Array[String], classifiers: Array[(String, Classifier)], regressors: Array[(String, Regressor)], preprocessor: Preprocessor) extends Serializable {

  val mapClassifiers = classifiers.toMap

  val mapRegressors = regressors.toMap

  val MISS_CLASSIFIED = -1

  val rootClassifier = getRootClassifier

  val rootRegressor = getRootRegressor

  private def getRootClassifier(): Double = {
    //FIXME restituisce -1 anche quando ci sono dei classificatori
    //FIXME controllare il valore ottenuto

    val classSeqSizeOne = classifiers.
      filter(kv => kv._1.split(" -1 ").size == 2)

    var classifier = classifiers(0)._2
    var maxNumExamples = classifier.trainingData.count()

    if (!classSeqSizeOne.isEmpty) {
      val classifierEmptySeq = classSeqSizeOne.foreach { kv =>
        val trainingSize = kv._2.trainingData.count()
        if (trainingSize > maxNumExamples) {
          classifier = kv._2
          maxNumExamples = trainingSize
        }
      }
      val labelCount: collection.Map[Double, Long] = classifier.trainingData.map(labeledPoint => labeledPoint.label).countByValue()
      val firstElement = labelCount.toList(0)
      var labeledClass = firstElement._1
      var maxLabeledExamples = firstElement._2
      labelCount.foreach { lc =>
        if (maxLabeledExamples < lc._2) {
          labeledClass = lc._1
          maxLabeledExamples = lc._2
        }
      }
      labeledClass
    }
    else {
      println("No Classifiers found")
      -1D
    }
  }

  private def getRootRegressor(): Double = {

    val regressSeqSizeOne = regressors.filter(kv => kv._1.split("-1").size == 2)

    var regressor = regressors(0)._2
    var maxNumExamples = regressor.trainingData.count()
    val classifierEmptySeq = regressSeqSizeOne.foreach { kv =>
      val trainingSize = kv._2.trainingData.count()
      if (trainingSize > maxNumExamples) {
        regressor = kv._2
        maxNumExamples = trainingSize
      }
    }
    val labelCount: collection.Map[Double, Long] = regressor.trainingData.map(labeledPoint => labeledPoint.label).countByValue()
    val firstElement = labelCount.toList(0)
    var labeledClass = firstElement._1
    var maxLabeledExamples = firstElement._2
    labelCount.foreach { lc =>
      if (maxLabeledExamples < lc._2) {
        labeledClass = lc._1
        maxLabeledExamples = lc._2
      }
    }
    labeledClass
  }

  private def classify(sequence: String): Double = {

    val tidsSequence = sequence.split(Dataset.ITEMSET_SEPARATOR).init
      .map(item => item.split(Dataset.ITEM_SEPARATOR)(0).trim)
      .mkString(Dataset.ITEMSET_SEPARATOR) + Dataset.ITEMSET_SEPARATOR + Dataset.SEQUENCE_SEPARATOR

    mapClassifiers.get(tidsSequence) match {

      case Some(classifier) =>
        val features = preprocessor.extractFeatureSequence(sequence)
        classifier.predict(features)

      case None =>
        var parentSeq = sequence
        var parentItemsSeq = tidsSequence

        while (!mapClassifiers.get(parentItemsSeq).isDefined && !parentItemsSeq.trim.equals(Dataset.SEQUENCE_SEPARATOR)) {
          val itemsSplit = parentItemsSeq.split(Dataset.ITEMSET_SEPARATOR).toList
          val seqSplit = parentSeq.split(Dataset.ITEMSET_SEPARATOR).toList
          val size = itemsSplit.length - 2
          parentItemsSeq = itemsSplit.patch(size, Nil, 1).mkString(Dataset.ITEMSET_SEPARATOR)
          parentSeq = seqSplit.patch(size, Nil, 1).mkString(Dataset.ITEMSET_SEPARATOR)
        }

        if (mapClassifiers.get(parentItemsSeq).isDefined) {
          val classifier = mapClassifiers.get(parentItemsSeq).get
          val features = preprocessor.extractFeatureSequence(parentSeq)
          classifier.predict(features)

        }
        else {
          //MISS_CLASSIFIED
          rootClassifier
        }

    }
  }

  private def predictRegressor(sequence: String): Double = {

    val tidsSequence = sequence.split(Dataset.ITEMSET_SEPARATOR).init
      .map(item => item.split(Dataset.ITEM_SEPARATOR)(0).trim)
      .mkString(Dataset.ITEMSET_SEPARATOR) + Dataset.ITEMSET_SEPARATOR + Dataset.SEQUENCE_SEPARATOR

    mapRegressors.get(tidsSequence) match {

      case Some(classifier) =>
        val features = preprocessor.extractFeatureSequence(sequence)
        classifier.predict(features)

      case None =>
        var parentSeq = sequence
        var parentItemsSeq = tidsSequence

        while (!mapRegressors.get(parentItemsSeq).isDefined && !parentItemsSeq.trim.equals(Dataset.SEQUENCE_SEPARATOR)) {
          val itemsSplit = parentItemsSeq.split(Dataset.ITEMSET_SEPARATOR).toList
          val seqSplit = parentSeq.split(Dataset.ITEMSET_SEPARATOR).toList
          val size = itemsSplit.length - 2
          parentItemsSeq = itemsSplit.patch(size, Nil, 1).mkString(Dataset.ITEMSET_SEPARATOR)
          parentSeq = seqSplit.patch(size, Nil, 1).mkString(Dataset.ITEMSET_SEPARATOR)
        }

        if (mapRegressors.get(parentItemsSeq).isDefined) {
          val classifier = mapRegressors.get(parentItemsSeq).get
          val features = preprocessor.extractFeatureSequence(parentSeq)
          classifier.predict(features)

        }
        else {
          //MISS_CLASSIFIED
          rootRegressor
        }

    }

  }

  def testEnsemble(lines: RDD[String], maxLength: Int): RDD[Prediction] = {

    val subSequences = lines.map { line =>

      val rawElements = line.split(Dataset.ITEMSET_SEPARATOR).dropRight(2)
      if (rawElements.size > maxLength)
        rawElements.take(maxLength)
      else rawElements

    }.flatMap { array =>

      for (i <- 1 until array.size) yield {
        val predictionElement = array(i).split(Dataset.ITEM_SEPARATOR)(1)
        val candidates = array.slice(0, i)
        val subSequence = candidates.mkString(" -1 ") + " -1 -2"

        //generate elements anchored to the last element
        //for List(1,2) generates Vector(List(1), List(1,2))
        val elements = for (j <- 1 to candidates.size)
          yield candidates.takeRight(j).mkString(" -1 ") + " -1 -2"

        (subSequence, i, elements, predictionElement)
      }
    }

    val results = subSequences.map {

      case (sequence, size, candidates, jsonLastItems) =>

        //predicted class majority vote
        val predictedClass = candidates
          .map(classify)
          .groupBy(identity)
          .maxBy(_._2.length)._1
        val predictedTime = candidates.map(predictRegressor)
        val avgPredictedTime = predictedTime.reduce(_ + _) / predictedTime.size

        val actualClass = preprocessor.getLabelClassification(jsonLastItems)
        val actualTime = preprocessor.getLabelRegression(jsonLastItems)

        Prediction(sequence, size, predictedClass, actualClass, avgPredictedTime, actualTime)

    }

    results
  }

  //########### TEST ENSEMBLE

  def test2(lines: RDD[String], maxLength: Int): RDD[Prediction] = {

    val subSequences = lines
      .map { l =>
        val splitted = l.split(Dataset.ITEMSET_SEPARATOR).dropRight(2)
        if (splitted.size > maxLength)
          splitted.take(maxLength)
        else splitted
      }
      .flatMap { splits =>
        val indexList = 1 to splits.size
        indexList.map { index =>
          val json = splits(index - 1).split(Dataset.ITEM_SEPARATOR)(1)
          val subSequence = splits.slice(0, index).mkString(" -1 ") + " -1 -2"
          (subSequence, index, json)
        }
      }

    val results = subSequences.map { s =>
      val predictedClass = classify(s._1)
      val timePredicted = predictRegressor(s._1)
      val jsonLastItems = s._3
      val actualClass = preprocessor.getLabelClassification(jsonLastItems)
      val actualTime = preprocessor.getLabelRegression(jsonLastItems)

      Prediction(s._1, s._2, predictedClass, actualClass, timePredicted, actualTime)
    }

    results
  }

  /**
   *
   * @param testingData
   * @return (s, classReal, classPredicted, timeReal, timePredicted), where <ul>
   *           <li>s = sequence</li>
   *           <li>classReal = real next class</li>
   *           <li>classPredicted = predicted next class</li>
   *           <li>timeReal = real completion time</li>
   *           <li>timePredicted = predicted completion time</li>
   *           </ul>
   */
  def test(testingData: RDD[String]): RDD[Prediction] = {
    //done 30/3/2015

    val subSequences = testingData.flatMap { sequence =>

      val splits = sequence.split(Dataset.ITEMSET_SEPARATOR).dropRight(1)
      val indexList = 1 to splits.size
      indexList.map { index =>
        val json = splits(index - 1).split(Dataset.ITEM_SEPARATOR)(1)
        val subSequence = splits.slice(0, index).mkString(" -1 ") + " -1 -2"
        (subSequence, index, json)
      }
    }

    val results = subSequences.map { s =>
      val predictedClass = classify(s._1)
      val timePredicted = predictRegressor(s._1)
      val jsonLastItems = s._3
      val actualClass = preprocessor.getLabelClassification(jsonLastItems)
      val actualTime = preprocessor.getLabelRegression(jsonLastItems)
      /* Codice Iovine */
      //println("sequence to predict is " + s._1)
      //println("real class is " + actualClass + ", predicted class is " + predictedClass)
      /*****************/
      Prediction(s._1, s._2, predictedClass, actualClass, timePredicted, actualTime)
    }

    results
  }

}

object PmmWithFast extends Serializable {

  /**
   *
   * @param associationAlgo
   * @param minSupp
   * @param confidence a value from 1 to 0.1, 1 means 100% and 0.1 means 10%
   * @param categoricalFeaturesInfo
   * @param input
   * @return a pair of array sequence -> classifier and  a array of sequence -> regressor
   */
  def train(associationAlgo: AssociationAlgorithm.Value,
            minSupp: Float,
            confidence: Float,
            categoricalFeaturesInfo: Map[Int, Int],
            input: RDD[String], preprocessor: Preprocessor) = {
    new PmmWithFast(associationAlgo, minSupp, confidence, categoricalFeaturesInfo, preprocessor).run(input, preprocessor)
  }

  def cumulativePerformance(predictions: RDD[Prediction]): RDD[Result] = {

    val diffsActualEstimated = predictions.map { pred =>
      val diffTime = math.pow((pred.realTime - pred.predictedTime), 2)

      /* Codice Iovine */
      //println("prediction of sequence " + pred.sequence)
      //println("realClass is " + pred.realClass + ", predictedClass is " + pred.predictedClass)
      /*               */
      val diffClass =
        if (pred.realClass == pred.predictedClass)
          1
        else 0
      (pred.sequenceSize, diffTime, diffClass)
    }

    val reduced = diffsActualEstimated.reduce { (a, b) =>

      val sumObservations = 1 + 1
      val sumDiffTime = a._2 + b._2
      val sumDiffClass = a._3 + b._3

      (sumObservations, sumDiffTime, sumDiffClass)
    }

    val count = diffsActualEstimated.count()
    val rmse = math.sqrt(reduced._2 / count)
    val accuracy = reduced._3.toDouble / count

    predictions.context.parallelize(Array(Result(1, count.toInt, rmse, accuracy)))
  }

  def computePerformance2(predictions: RDD[Prediction]): RDD[Result] = {

    val diffsActualEstimated = predictions.map { pred =>
      val diffTime = math.pow((pred.realTime - pred.predictedTime), 2)
      val diffClass =
        if (pred.realClass == pred.predictedClass)
          1
        else 0
      (pred.sequenceSize, diffTime, diffClass)
    }

    val groupBySeqLength = diffsActualEstimated.groupBy(x => x._1)

    groupBySeqLength.map {

      case (seqLength, elements) =>

        val sumValues = elements.reduce { (el1, el2) =>

          val sumTime = el1._2 + el2._2
          val sumClass = el1._3 + el2._3

          (seqLength, sumTime, sumClass)
        }

        val size = elements.size
        val rmse = math.sqrt(sumValues._2 / size)
        val accuracy = sumValues._3.toDouble / size

        Result(seqLength, size, rmse, accuracy)
    }
  }

  /**
   *
   * @param predictions
   * @return RDD having as key the length of the sequence and as value a Result Object
   */
  def computePerformance(predictions: RDD[Prediction]): RDD[Result] = {

    val diffRegression: RDD[(Int, Double)] = predictions.map { prediction =>
      val diffTime: Double = prediction.realTime - prediction.predictedTime
      (prediction.sequenceSize, diffTime)
    }
    val diffClassification: RDD[(Int, Double)] = predictions.map { prediction =>
      val diffUser: Double = prediction.realClass - prediction.predictedClass
      (prediction.sequenceSize, diffUser)
    }

    val regressionMap = diffRegression.combineByKey(
      (v) => (v, 1), //createCombiner
      (acc: (Double, Int), v) => (acc._1 + math.pow(v, 2), acc._2 + 1), //mergeValue
      (acc1: (Double, Int), acc2: (Double, Int)) => (acc1._1 + acc2._1, acc1._2 + acc2._2) //mergeCombiners
    ).map {
        case (key, value) =>
          (key, math.sqrt(value._1 / value._2.toFloat))
      }.collectAsMap()

    val resClassification: RDD[(Int, (Double, Int))] = diffClassification.combineByKey(
      (v) => if (v == 0d) (1D, 1) else (0D, 1), //createCombiner
      (acc: (Double, Int), v) => if (v == 0d) (acc._1 + 1, acc._2 + 1) else (acc._1, acc._2 + 1), //mergeValue
      (acc1: (Double, Int), acc2: (Double, Int)) => (acc1._1 + acc2._1, acc1._2 + acc2._2) //mergeCombiners
    ).map {
        case (key, value) => (key, (value._1 / value._2.toFloat, value._2))
      }

    val results = resClassification.map {
      case (k, value) =>
        val rmse = regressionMap.getOrElse(k, 0D)
        Result(k, value._2, rmse, value._1)
    }
    results
  }

  def computePerformanceWithRRMSE(predictions: RDD[Prediction]): RDD[Result2] = {

    val diffRegression: RDD[(Int, Double)] = predictions.map { prediction =>
      val diffTime: Double = prediction.realTime - prediction.predictedTime
      (prediction.sequenceSize, diffTime)
    }

    //Iovine: List of tuples (length, realTime)
    val realTimeMap: RDD[(Int, Double)] = predictions.map { prediction =>
      (prediction.sequenceSize, prediction.realTime)
    }


    val diffClassification: RDD[(Int, Double)] = predictions.map { prediction =>
      val diffUser: Double = prediction.realClass - prediction.predictedClass
      (prediction.sequenceSize, diffUser)
    }

    val regressionMap = diffRegression.combineByKey(
      (v) => (v, 1), //createCombiner
      (acc: (Double, Int), v) => (acc._1 + math.pow(v, 2), acc._2 + 1), //mergeValue
      (acc1: (Double, Int), acc2: (Double, Int)) => (acc1._1 + acc2._1, acc1._2 + acc2._2) //mergeCombiners
    ).map {
      case (key, value) =>
        (key, math.sqrt(value._1 / value._2.toFloat))
    }.collectAsMap()

    //Iovine: Calculate the average realTime for each length
    val averageTimeMap = realTimeMap.combineByKey(
      (v) => (v, 1),
      (acc: (Double, Int), v) => (acc._1 + v, acc._2 + 1),
      (acc1: (Double, Int), acc2: (Double, Int)) => (acc1._1 + acc2._1, acc1._2 + acc2._2)
    ).map {
      case (key, value) =>
        (key, value._1 / value._2)
    }.collectAsMap()

    //Iovine: Differences between real time and average time
    val diffAverage: RDD[(Int, Double)] = predictions.map {prediction =>
      val diffTime = prediction.realTime - averageTimeMap(prediction.sequenceSize)
      (prediction.sequenceSize, diffTime)
    }

    val stdDevMap = diffAverage.combineByKey(
      (v) => (v, 1), //createCombiner
      (acc: (Double, Int), v) => (acc._1 + math.pow(v, 2), acc._2 + 1), //mergeValue
      (acc1: (Double, Int), acc2: (Double, Int)) => (acc1._1 + acc2._1, acc1._2 + acc2._2) //mergeCombiners
    ).map {
      case (key, value) =>
        (key, math.sqrt(value._1 / value._2.toFloat))
    }.collectAsMap()

    var rrmseMap = new mutable.HashMap[Int, Double]()

    for (key <- regressionMap.keys) {
      rrmseMap += ((key, regressionMap(key) / stdDevMap(key)))
    }


    val resClassification: RDD[(Int, (Double, Int))] = diffClassification.combineByKey(
      (v) => if (v == 0d) (1D, 1) else (0D, 1), //createCombiner
      (acc: (Double, Int), v) => if (v == 0d) (acc._1 + 1, acc._2 + 1) else (acc._1, acc._2 + 1), //mergeValue
      (acc1: (Double, Int), acc2: (Double, Int)) => (acc1._1 + acc2._1, acc1._2 + acc2._2) //mergeCombiners
    ).map {
      case (key, value) => (key, (value._1 / value._2.toFloat, value._2))
    }

    val results = resClassification.map {
      case (k, value) =>
        val rmse = regressionMap.getOrElse(k, 0D)
        val rrmse = rrmseMap(k)
        Result2(k, value._2, rmse, rrmse, value._1)
    }
    results
  }
}

