package it.kdde.distributed.util

import java.nio.file.{ Paths, Path }
import java.util.Calendar

import it.kdde.distributed.sequentialpatterns.PmmWithFastModel
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
 * Created by fabiana on 3/31/15.
 */
object Util {

  case class Prediction(
    sequence: String,
    sequenceSize: Int,
    predictedClass: Double,
    realClass: Double,
    predictedTime: Double,
    realTime: Double)
  //sizeSeq = length of
  // the sequences, numSeq = numbers of sequences having length sizeSeq, rmse and accuracy are respectively the rmse and accuracy mean of all sequences having that lenght
  case class Result(
    sizeSeq: Int,
    numSeq: Int,
    rmse: Double,
    accuracy: Double)

  //TEST IOVINE
  case class Result2(
                     sizeSeq: Int,
                     numSeq: Int,
                     rmse: Double,
                     rrmse: Double,
                     accuracy: Double)

  def printStat(inputFile: String,
                numPartitions: String,
                minSupp: String,
                confidence: String,
                results: RDD[Result],
                time: Long,
                pmmWithFastModel: PmmWithFastModel,
                statisticsFile: String)(implicit sc: SparkContext): Unit = {

    val header = "Data in Millisec,InputFile,Partitions,minSupp,confidence,seq. Length, number Regressors, number Classifiers ,num. Seq., RMSE, accuracy, time"
    val statsFile: Path = Paths.get(statisticsFile)

    //store the number of classifiers for layer
    val mapClassifiersLayer: Map[Int, Int] = pmmWithFastModel.mapClassifiers.keys.toList.
      map { kv => (kv.split("-1").init.size, 1) }.foldLeft(Map.empty[Int, Int]) {
        (acc, pair) => acc + (pair._1 -> (acc.getOrElse(pair._1, 0) + 1))
      }

    //store the number of classifiers for layer
    val mapRegressorsLayer: Map[Int, Int] = pmmWithFastModel.mapRegressors.keys.toList.
      map { kv => (kv.split("-1").init.size, 1) }.foldLeft(Map.empty[Int, Int]) {
        (acc, pair) => acc + (pair._1 -> (acc.getOrElse(pair._1, 0) + 1))
      }

    val mapRMSE = {
      results.map(r => (r.sizeSeq, r.rmse))
    }.collectAsMap()

    val mapAccuracy = {
      results.map(r => (r.sizeSeq, r.accuracy))
    }.collectAsMap()

    val mapNumberSequences = {
      results.map(r => (r.sizeSeq, r.numSeq))
    }.collectAsMap()

    val listResults: List[String] = mapRMSE.keys.toList.sorted.map { k =>
      val rmseRound = roundAt(mapRMSE.get(k).get, 2)
      val accuracyRound = roundAt(mapAccuracy.get(k).get, 2)

      val numberClassifiers = mapClassifiersLayer.getOrElse(k, 0)
      val numberRegressors = mapRegressorsLayer.getOrElse(k, 0)

      val string = Calendar.getInstance().getTimeInMillis() + "," +
        inputFile + "," +
        numPartitions + "," +
        minSupp + "," +
        confidence + "," +
        k + "," +
        numberRegressors + "," +
        numberClassifiers + "," +
        mapNumberSequences.getOrElse(k, -1) + "," +
        rmseRound + "," +
        accuracyRound + "," +
        time

      string
    }
    val listWithHeader = header :: listResults
    val rddRes = sc.parallelize(listWithHeader)
    rddRes.saveAsTextFile(statisticsFile)
  }

  //TEST IOVINE
  def printStatWithRRMSE(inputFile: String,
                numPartitions: String,
                minSupp: String,
                confidence: String,
                results: RDD[Result2],
                time: Long,
                pmmWithFastModel: PmmWithFastModel,
                statisticsFile: String)(implicit sc: SparkContext): Unit = {

    val header = "Data in Millisec,InputFile,Partitions,minSupp,confidence,seq. Length, number Regressors, number Classifiers ,num. Seq., RMSE, RRMSE, accuracy, time"
    val statsFile: Path = Paths.get(statisticsFile)

    //store the number of classifiers for layer
    val mapClassifiersLayer: Map[Int, Int] = pmmWithFastModel.mapClassifiers.keys.toList.
      map { kv => (kv.split("-1").init.size, 1) }.foldLeft(Map.empty[Int, Int]) {
      (acc, pair) => acc + (pair._1 -> (acc.getOrElse(pair._1, 0) + 1))
    }

    //store the number of classifiers for layer
    val mapRegressorsLayer: Map[Int, Int] = pmmWithFastModel.mapRegressors.keys.toList.
      map { kv => (kv.split("-1").init.size, 1) }.foldLeft(Map.empty[Int, Int]) {
      (acc, pair) => acc + (pair._1 -> (acc.getOrElse(pair._1, 0) + 1))
    }

    val mapRMSE = {
      results.map(r => (r.sizeSeq, r.rmse))
    }.collectAsMap()

    val mapRRMSE = {
      results.map(r => (r.sizeSeq, r.rrmse))
    }.collectAsMap()

    val mapAccuracy = {
      results.map(r => (r.sizeSeq, r.accuracy))
    }.collectAsMap()

    val mapNumberSequences = {
      results.map(r => (r.sizeSeq, r.numSeq))
    }.collectAsMap()

    val listResults: List[String] = mapRMSE.keys.toList.sorted.map { k =>
      val rmseRound = roundAt(mapRMSE.get(k).get, 2)
      val accuracyRound = roundAt(mapAccuracy.get(k).get, 2)
      val rrmseRound = roundAt(mapRRMSE.get(k).get, 2)

      val numberClassifiers = mapClassifiersLayer.getOrElse(k, 0)
      val numberRegressors = mapRegressorsLayer.getOrElse(k, 0)

      val string = Calendar.getInstance().getTimeInMillis() + "," +
        inputFile + "," +
        numPartitions + "," +
        minSupp + "," +
        confidence + "," +
        k + "," +
        numberRegressors + "," +
        numberClassifiers + "," +
        mapNumberSequences.getOrElse(k, -1) + "," +
        rmseRound + "," +
        rrmseRound + "," +
        accuracyRound + "," +
        time

      string
    }
    val listWithHeader = header :: listResults
    val rddRes = sc.parallelize(listWithHeader)
    rddRes.saveAsTextFile(statisticsFile)
  }

  def roundAt(n: Double, p: Int): Double = {
    val s = math pow (10, p); (math round n * s) / s
  }
}
