package it.kdde.distributed.preprocessing

import it.dtk.stackoverflow.Model.Answer
import it.dtk.stackoverflow.StackExchangeDataset
import it.kdde.distributed.sequentialpatterns.Dataset
import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.json4s._
import org.json4s.jackson.JsonMethods._


/**
 * Created by fabiofumarola on 24/03/15.
 */
object StackExchangePreprocessor extends Preprocessor with Serializable {

  val featuresToIgnore = List(15, 16, 17, 18)

  //  case class Answer(
  //                    reputation: Int,
  //                    creationDate: Long,
  //                    displayName: Int,
  //                    lastAccessDate: Long,
  //                    websiteUrl: Int,
  //                    location: Int,
  //                    aboutMe: Int,
  //                    views: Int,
  //                    upVotes: Int,
  //                    downVotes: Int,
  //                    profileImageUrl: Int,
  //                    age: Int,
  //                    postScore: Int,
  //                    postViewCount: Int,
  //                    commentCount: Int,
  //                    classificationValue: Int,
  //                    regressionValue: Double,
  //                    sid: Int,
  //                  answerId: Int, //id of the answer)


  /**
   * Given a threshold minLenght return a RDD of String which represents the starting dataset, composed by sequence having size > minLenght
   */
  override def generateSequenceDataset(lines: RDD[String], minLength: Int)(implicit sc: SparkContext): RDD[String] =
    StackExchangeDataset.generateSequenceDataset(minLength)

  /**
   * *
   * @param row in the form userid1 :: {samedata} -1 userid2 :: {samedata} -1 -2
   * @return the vector of feature associate to sequence row
   */
  def extractFeatureSequence(row: String): Vector = {
    implicit val formats = DefaultFormats
    //remove last item -2
    val itemsets = row.split(Dataset.ITEMSET_SEPARATOR).toList.init

    val rowVector = itemsets.flatMap { item =>
      val jsondata = item.split(" :: ")(1).trim
      val answer = parse(jsondata).extract[Answer]
      val r = Answer.unapply(answer).get
      val indexedValues: List[(Any, Int)] = r.productIterator.toList.zip(0 until r.productArity)

      val filteredVector = indexedValues.map { kv =>
        if (!featuresToIgnore.contains(kv._2)) {
          Some(kv._1.toString.toDouble)
        }
        else {
          None
        }
      }

      //map(_.toString.toDouble)

      filteredVector.flatten
    }.toArray


    Vectors.dense(rowVector)

  }

  override def numberFeatures: Int = StackExchangeDataset.numFeatures -  featuresToIgnore.size

  override def categoricalFeatures: Map[Int, Int] = StackExchangeDataset.categoricalFeatures

  /**
   * Vengono usati nella classe Predictors per generare i classificatori per ogni nodo dell'albero
   * @param row
   * @return
   */
  override def getLabeledPointForClassification(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats // Brings in default date formats etc for Json4s.
    val rowLabel = parse(row.last.getData).extract[Answer]
    val label = rowLabel.classificationValue.toDouble

    val rowVector = row.flatMap { listNode =>
      val data = parse(listNode.getData).extract[Answer]
      val r = Answer.unapply(data).get
      val indexedValues: List[(Any, Int)] = r.productIterator.toList.zip(0 until r.productArity)

      val filteredVector = indexedValues.map { kv =>
        if (!featuresToIgnore.contains(kv._2)) {
          Some(kv._1.toString.toDouble)
        }
        else {
          None
        }
      }
      filteredVector.flatten
    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector.toArray))
  }

  /**
   * Vengono usati nella classe Predictors per generare i regressori per ogni nodo dell'albero
   * @param row
   * @return
   */
  override def getLabeledPointForRegression(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    val rowLabel: Answer = parse(row.last.getData).extract[Answer]
    val label = rowLabel.classificationValue.toDouble

    val rowVector = row.flatMap { listNode =>
      val data = parse(listNode.getData).extract[Answer]
      val r = Answer.unapply(data).get
      val indexedValues: List[(Any, Int)] = r.productIterator.toList.zip(0 until r.productArity)

      val filteredVector = indexedValues.map { kv =>
        if (!featuresToIgnore.contains(kv._2)) {
          Some(kv._1.toString.toDouble)
        }
        else {
          None
        }
      }

      filteredVector.flatten
    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  /**
   *
   * @param json return the value of the labelClassification contained in the json
   */
  override def getLabelClassification(json: String): Double = {
    implicit val formats = DefaultFormats
    val answer: Answer = parse(json).extract[Answer]
    answer.classificationValue.toDouble
  }

  override def getLabelRegression(json: String): Double = {
    implicit val formats = DefaultFormats
    val answer: Answer = parse(json).extract[Answer]
    answer.regressionValue
  }
}
