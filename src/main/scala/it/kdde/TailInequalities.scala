package it.kdde

/**
 * Created by fabiofumarola on 10/02/15.
 */
object TailInequalities {

  def chernoffBound(minSup: Float, confidence: Float, datasetSize: Long): Double = {

    require(confidence <= 1.0F, s" $confidence confidence cannot be greater than 1")
    require(confidence >= 0.1F, s" $confidence confidence cannot be lower than 1")

    if (confidence == 1)
      0
    else {
      val bound = (2 * minSup * math.log1p(2 / confidence)) / datasetSize
      math.sqrt(bound)
    }
  }

  def hoeffdingBound(minSup: Float, confidence: Float, datasetSize: Long): Double = ???

}
