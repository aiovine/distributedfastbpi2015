package it.datacleaning

import org.apache.spark.{ SparkContext, SparkConf }
import org.scalatest._

import scala.util.Properties

/**
 * Created by fabiofumarola on 26/03/15.
 */
//class PreProcessingTest extends FlatSpec with Matchers with OptionValues with Inside with Inspectors {
//
//  val master = Properties.envOrElse("MASTER", "local") //Using local as the default value for the master machine makes it easy to launch your application locally in a test environment.
//  val conf = new SparkConf().setAppName("PreProcessing")
//  implicit val sc = new SparkContext(master, "PreProcessing", conf)
//
//  "a PreProcessing module " should "transform a given RDD of values " in {
//
//    lazy val array = (1 to 1000).map(v => Array(v.toString))
//    val rdd = sc.parallelize(array)
//
//    val (rddResult, mapResult) = PreProcessing.nominalToOrdinal(rdd, 0)
//
//    rddResult.count() should be (1000L)
//
//    mapResult should have size  1000L
//
//  }
//}
