package it.kdde.distributed.sequentialpatterns

/**
 * Created by fabiana on 2/9/15.
 */

//import org.scalatest.FlatSpec
//
//class DistributedContinuousFastTest extends FlatSpec {
//
//  behavior of "The Foowing dataset \n c#pro:va#14 -1 a#pro:va#13 -1 b#pro:va#12 -1 c#pro:va#11 -1 b#pro:va#10 -1 e#pro:va#9 -1 a#pro:va#8 -1 c#pro:va#7 -1 e#pro:va#6 -1 b#pro:va#5 -1 a#pro:va#4 -1 c#pro:va#3 -1 b#pro:va#2 -1 a#pro:va#1 -1 -2 \nc#pro:va#14 -1 a#pro:va#13 -1 b#pro:va#12 -1 c#pro:va#11 -1 b#pro:va#10 -1 f#pro:va#9 -1 a#pro:va#8 -1 c#pro:va#7 -1 g#pro:va#6 -1 b#pro:va#5 -1 a#pro:va#4 -1 c#pro:va#3 -1 b#pro:va#2 -1 a#pro:va#1 -1 -2 \nc#pro:va#14 -1 a#pro:va#13 -1 b#pro:va#12 -1 c#pro:va#11 -1 b#pro:va#10 -1 h#pro:va#9 -1 a#pro:va#8 -1 c#pro:va#7 -1 i#pro:va#6 -1 b#pro:va#5 -1 a#pro:va#4 -1 c#pro:va#3 -1 b#pro:va#2 -1 a#pro:va#1 -1 -2 \nc#pro:va#14 -1 a#pro:va#13 -1 b#pro:va#12 -1 c#pro:va#11 -1 b#pro:va#10 -1 z#pro:va#9 -1 a#pro:va#8 -1 c#pro:va#7 -1 w#pro:va#6 -1 b#pro:va#5 -1 a#pro:va#4 -1 c#pro:va#3 -1 b#pro:va#2 -1 a#pro:va#1 -1 -2"
//  val dataset: List[String] =
//    List("c#pro:va#1 -1 a#pro:va#2 -1 b#pro:va#3 -1 c#pro:va#4 -1 b#pro:va#5 -1 e#pro:va#6 -1 a#pro:va#7 -1 c#pro:va#8 -1 e#pro:va#9 -1 b#pro:va#10 -1 a#pro:va#11 -1 c#pro:va#12 -1 b#pro:va#13 -1 a#pro:va#40 -1 -2",
//      "c#pro:va#1 -1 a#pro:va#2 -1 b#pro:va#3 -1 c#pro:va#4 -1 b#pro:va#5 -1 f#pro:va#6 -1 a#pro:va#7 -1 c#pro:va#8 -1 g#pro:va#9 -1 b#pro:va#10 -1 a#pro:va#11 -1 c#pro:va#12 -1 b#pro:va#13 -1 a#pro:va#40 -1 -2",
//      "c#pro:va#1 -1 a#pro:va#2 -1 b#pro:va#3 -1 c#pro:va#4 -1 b#pro:va#5 -1 h#pro:va#6 -1 a#pro:va#7 -1 c#pro:va#8 -1 i#pro:va#9 -1 b#pro:va#10 -1 a#pro:va#11 -1 c#pro:va#12 -1 b#pro:va#13 -1 a#pro:va#40 -1 -2",
//      "c#pro:va#1 -1 a#pro:va#2 -1 b#pro:va#3 -1 c#pro:va#4 -1 b#pro:va#5 -1 z#pro:va#6 -1 a#pro:va#7 -1 c#pro:va#8 -1 w#pro:va#9 -1 b#pro:va#10 -1 a#pro:va#11 -1 c#pro:va#12 -1 b#pro:va#13 -1 a#pro:va#40 -1 -2")
//  val distributedDataset = Dataset.load(dataset, 0.5f, 1f)
//
//  val fast = DistributedContinuousFast(distributedDataset)
//  it should "have 21 consecutive sequences" in {
//
//    assert(fast.getContiguousSequences.size === 21)
//  }
//
//  it should "have 5 opening sequences" in {
//    assert(fast.getOpeningSequences.size === 5)
//  }
//}
