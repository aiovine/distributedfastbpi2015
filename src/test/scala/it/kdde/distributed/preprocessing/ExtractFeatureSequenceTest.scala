package it.kdde.distributed.preprocessing

import it.dtk.stackoverflow.Model.Answer
//import it.kdde.distributed.preprocessing.StackExchangePreprocessor.Answer
import org.json4s.jackson.JsonMethods._
import org.json4s.{DefaultFormats, Extraction}

/**
 * Created by fabiana on 3/24/15.
 */
object Prova extends App(){

  implicit val formats = DefaultFormats

  val ans1 = Answer(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16, 17, 18, 19)
  val ans2 = Answer(11,22,33,44,55,66,77,88,99,100,120,130,140,150,160,170, 180,190, 200)
  val jValue1 = Extraction.decompose(ans1)
  val string1 = compact(render(jValue1))
  val jValue2 = Extraction.decompose(ans2)
  val string2 = compact(render(jValue2))

  val string = "a :: "+string1+" -1 b :: "+string2+" -1 -2"
  println(StackExchangePreprocessor.extractFeatureSequence(string))


  val string3: String = """{"sid":14231,"answerId":44268,"reputation":0,"creationDate":1366723316467,"displayName":1,"lastAccessDate":1410526936733,"websiteUrl":1,"location":1,"aboutMe":1,"views":2002,"upVotes":1477,"downVotes":30,"profileImageUrl":0,"age":28,"postScore":2,"postViewCount":0,"commentCount":0,"classificationValue":12442,"regressionValue":1.882335824E10} """

  val prova = parse(string3).extract[Answer]
  val jobj = parse(string3)
  println("Caso 1 ":+ prova.age)
}
