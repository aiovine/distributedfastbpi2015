package it.kdde

//import it.kdde.distributed.fast.{DistributedDataset, DistributedFast}
import org.scalatest.FlatSpec

/**
 * Created by fabiana on 2/3/15.
 */
//class DistributedFastTest extends FlatSpec {
 /*
  behavior of "A distributed Fast algorithm"

  it should "have 142 frequent sequences for the following dataset \n 1 5 -1 3 -1 4 -1 1 -1 -2\n 4 -1 1 -1 2 -1 -2\n 4 -1 1 2 5 -1 2 3 4 -1 -2" in {
    val dataset = List("1 5 -1 3 -1 4 -1 1 -1 -2", "4 -1 1 -1 2 -1 -2", "4 -1 1 2 5 -1 2 3 4 -1 -2 \n and min_sup=0")
    val distributedDataset = DistributedDataset(dataset, 0)
    val distributedFast = DistributedFast(distributedDataset)
    val res = distributedFast.getFrequentSequences()
    assert(res.length === 142)
  }
  */
//}
