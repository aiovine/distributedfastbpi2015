organization := "it.kdde"

name := "distributedFast"

version := "1.0"

scalaVersion := "2.11.8"

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Local Maven Repository" at "file:///C:/Users/isz_d/.m2/repository",
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases")

libraryDependencies ++= Seq(
  //"org.apache.spark" %% "spark-core" % "1.3.0" % "provided",
  "org.apache.spark" %% "spark-core" % "1.6.0",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.apache.spark" %% "spark-mllib" % "1.6.0",
  "joda-time" % "joda-time" % "2.7",
  "org.json4s" %% "json4s-jackson" % "3.2.10",
  "it.nerdammer.bigdata" % "spark-hbase-connector_2.10" % "1.0.3", //excludeAll ExclusionRule(organization = "javax.servlet") excludeAll ExclusionRule(organization = "org.mortbay.jetty"),
  "org.apache.hbase" % "hbase-common" % "1.3.1" excludeAll ExclusionRule(organization = "org.mortbay.jetty") excludeAll ExclusionRule(organization = "javax.servlet"),
  "org.apache.hbase" % "hbase-client" % "1.3.1" excludeAll ExclusionRule(organization = "org.mortbay.jetty") excludeAll ExclusionRule(organization = "javax.servlet"),
  "org.apache.hbase" % "hbase-server" % "1.3.1" excludeAll ExclusionRule(organization = "org.mortbay.jetty") excludeAll ExclusionRule(organization = "javax.servlet")
//  "org.apache.hbase" % "hbase-common" % "1.0.0" % "provided",
//  "org.apache.hbase" % "hbase-client" % "1.0.0" % "provided",
//  "org.apache.hbase" % "hbase-server" % "1.0.0" % "provided"
)

assemblyOption in assembly :=
  (assemblyOption in assembly).value.copy(includeScala = false)