
 a :: "{ "key" : "value"}" -1 b :: "{ "key" : "value"}" -1 c :: "{ "key" : "value"}" -1 -2



##Distributed FAST

Verranno descritte 4 fasi fondamentali.

**1. Modello computazionale Spark**. 

1.1Vedere se esistono algoritmi di sequential pattern mining già implementati in Spark. 

1.2. Vedere come son implementati gli algoritmi di data mining in MLib.

**2. Dataset da utilizzare**

2.1. Ricerca di dataset relativi al web mining (web usage mining);

**3. Modello di predizione**

3.1. Goal: Predire la pagina web successiva, data una sequenza di pagine web già visualizzate; 

3.2. Predizione della durata di una sessione. Un'interessante applicazione potrebbe essere la predizione della pagina web che massimizza la durata della sessione.

**4. Ensamble Learning**

**5. Future Work:**

5.1. Studiare "statistical inequality" 

5.2. Studiare funzione di aggregazione dei risultati parziali e interpolazione dei valori indefiniti.


##Per utilizzare i dati di stackexchange:

1. avviare hbase ( ./hbase-0.98.10.1-hadoop2/bin/start-hbase.sh)

2. caricare i dati da stackexchange (run StackExchange Main)